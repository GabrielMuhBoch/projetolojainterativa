CREATE TABLE IF NOT EXISTS fabricantes (
    id serial primary key,
    nome VARCHAR(50),
    categoria1 VARCHAR(25),
    categoria2 VARCHAR(25),
    categoria3 VARCHAR(25)
);
CREATE TABLE IF NOT EXISTS produtos (
    id serial primary key,
    nome VARCHAR(50),
    fabricante VARCHAR(50),
    cat_fabricante VARCHAR(25),
    preco VARCHAR(10)
);
