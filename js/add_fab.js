$(document).ready(function () {


    $("#bAdicionar").click(function () {
        if (validar()) {
            fLocalComunicaServidor();
        }
        return false;
    });
});

function validar() {

    let nome = document.getElementById('nome').value;
    let categoria1 = document.getElementById('categoria1').value;
    let categoria2 = document.getElementById('categoria2').value;
    let categoria3 = document.getElementById('categoria3').value;

    if (nome == '' || categoria1 == '' || categoria2 == '' || categoria3 == '') {
        alert('preencha corretamente os campos');
        return false;
    }

    return true;
}



function fLocalComunicaServidor() {
    $.ajax({
        type: "POST",
        dataType: "json",
        url: "../php/add_fab.php",
        data: {
            nome: $('#nome').val(),
            categoria1: $('#categoria1').val(),
            categoria2: $('#categoria2').val(),
            categoria3: $('#categoria3').val(),
        },
        success: function (retorno) {
            if (retorno.funcao == "cadastrar") {
                if (retorno.status == "true") {
                    document.getElementById('form-fab').reset();
                }
            }
            alert(retorno.mensagem);
        }
    });
}
