$(document).ready(function () {

    $("#bAdicionar").click(function () {
        if (validar()) {
            fLocalComunicaServidor();
        }
        return false;
    });
});

function validar() {
    let nome = document.getElementById('nome').value;
    let preco = document.getElementById('preco').value;

    if (nome == '' || document.getElementById('select_fabricante').value == null || document.getElementById('select_categoria').value == null || preco == '') {
        alert('preencha corretamente os campos');
        return false;
    }

    return true;
}



function fLocalComunicaServidor() {
    $.ajax({
        type: "POST",
        dataType: "json",
        url: "../php/add_prod.php",
        data: {
            nome: $('#nome').val(),
            select_fabricante: $('#select_fabricante').val(),
            select_categoria: $('#select_categoria').val(),
            preco: $('#preco').val(),
        },
        success: function (retorno) {
            if (retorno.funcao == "cadastrar") {
                if (retorno.status == "true") {
                    document.getElementById('form-prod').reset();
                }
            }
            alert(retorno.mensagem);
        }
    });
}
